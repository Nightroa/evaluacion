FROM python:3.9
ENV PYTHONUNBUFFERED 1

# Install python libraries
COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install -r requirements.txt && \
    rm  requirements.txt

# Copy the source code
RUN mkdir /evaluacion
COPY ./evaluacion /evaluacion
WORKDIR /evaluacion