#!/bin/bash
python ./manage.py makemigrations &&
python ./manage.py migrate &&
python ./api/upload_data.py &&
python ./manage.py runserver 0.0.0.0:8000