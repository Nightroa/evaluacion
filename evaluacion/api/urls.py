from django.urls import path, include
from .views import EmployeeView, CityView, CompanyView, DepartmentView

urlpatterns = [
    path('employee/', EmployeeView.as_view(), name='employee'),
    path('employee/<int:id>', EmployeeView.as_view(), name='employee_methods'),
    path('city/', CityView.as_view(), name='city'),
    path('city/<int:id>', CityView.as_view(), name='city_methods'),
    path('company/', CompanyView.as_view(), name='company'),
    path('company/<int:id>', CompanyView.as_view(), name='company_methods'),
    path('department/', DepartmentView.as_view(), name='department'),
    path('department/<int:id>', DepartmentView.as_view(), name='department_methods'),
]