from django.http import JsonResponse
from django.views import View
from .models import Employee, Citys, Company, Departments
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import pandas as pd
import json

# Create your views here.

class EmployeeView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id>0:
            employees = list(Employee.objects.filter(id=id).values())
            if len(employees)>0:
                employee = employees[0]
                data = {"Employee":employee}
            else:
                data = {"message":"Employee no found"}
            return JsonResponse(data)
        else:
            employees = list(Employee.objects.values())
            if len(employees) >0:
                data = {'Employees':employees}
            else:
                data = {'message':"Employees no found"}
            return JsonResponse(data)

    def post(self, request):
        data = json.loads(request.body)
        first_name = data['first_name']
        last_name = data['last_name']
        address = data['address']
        zip = data['zip']
        phone1 = data['phone1']
        phone2 = data['phone2']
        email = data['email']
        city_id = data['city_id']
        company_id = data['company_id']
        department_id = data['department_id']

        Employee.objects.create(first_name = first_name,last_name =last_name,address =address,zip =zip,phone1 =phone1,phone2 =phone2, email =email,city_id = city_id,company_id = company_id,department_id = department_id)

        return JsonResponse({'Employees': first_name+' upload'})

    def put(self, request, id=0):
        data = json.loads(request.body)
        employees = list(Employee.objects.filter(id=id).values())
        if len(employees)>0:
            employee = Employee.objects.get(id=id)
            employee.first_name = data['first_name']
            employee.last_name = data['last_name']
            employee.address = data['address']
            employee.zip = data['zip']
            employee.phone1 = data['phone1']
            employee.phone2 = data['phone2']
            employee.email = data['email']
            employee.city_id = data['city_id']
            employee.company_id = data['company_id']
            employee.department_id = data['department_id']
            employee.save()
            data = {'message':'Employee edited'}
        else:
            data = {'message':'Employee no found'}
        return JsonResponse(data)

    def delete(self, request, id=0):
        employee = list(Employee.objects.filter(id=id).values())
        if len(employee)>0:
            Employee.objects.filter(id=id).delete()
            data = {"message": "Employeed deleted"}
        else:
            data = {'message':"Employee no found"}
        return JsonResponse(data)
        

class CityView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id>0:
            citys = list(Citys.objects.filter(id=id).values())
            if len(citys)>0:
                city = citys[0]
                data = {"City":city}
            else:
                data = {"message":"City no found"}
            return JsonResponse(data)
        else:
            citys = list(Citys.objects.values())
            if len(citys) >0:
                data = {'Citys':citys}
            else:
                data = {'message':"Citys no found"}
            return JsonResponse(data)

    def post(self, request):
        data = json.loads(request.body)
        city = data['city']
        state = data['state']
        Citys.objects.create(city = city, state =state)
        return JsonResponse({'City': city+'upload'})

    def put(self, request, id=0):
        data = json.loads(request.body)
        citys = list(Citys.objects.filter(id=id).values())
        if len(citys)>0:
            city = Citys.objects.get(id=id)
            city.city = data['city']
            city.state = data['state']
            city.save()
            data = {'message':'City edited'}
        else:
            data = {'message':'City no found'}
        return JsonResponse(data)

    def delete(self, request, id=0):
        city = list(Citys.objects.filter(id=id).values())
        if len(city)>0:
            Citys.objects.filter(id=id).delete()
            data = {"message": "City deleted"}
        else:
            data = {'message':"City no found"}
        return JsonResponse(data)

class CompanyView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id>0:
            companys = list(Company.objects.filter(id=id).values())
            if len(companys)>0:
                company = companys[0]
                data = {"Company":company}
            else:
                data = {"message":"Company no found"}
            return JsonResponse(data)
        else:
            companys = list(Company.objects.values())
            if len(companys) >0:
                data = {'Companys':companys}
            else:
                data = {'message':"Companys no found"}
            return JsonResponse(data)

    def post(self, request):
        data = json.loads(request.body)
        company_name = data['company_name']
        department_id =data['department_id']
        Company.objects.create(company_name = company_name, department_id =department_id)
        return JsonResponse({'Company': company_name+" upload"})

    def put(self, request, id=0):
        data = json.loads(request.body)
        companys = list(Company.objects.filter(id=id).values())
        if len(companys)>0:
            company = Company.objects.get(id=id)
            company.company_name = data['company_name']
            company.department_id = data['department_id']
            company.save()
            data = {'message':'Company edited'}
        else:
            data = {'message':'Company no found'}
        return JsonResponse(data)
        

    def delete(self, request, id=0):
        company = list(Company.objects.filter(id=id).values())
        if len(company)>0:
            Company.objects.filter(id=id).delete()
            data = {"message": "Company deleted"}
        else:
            data = {'message':"Company no found"}
        return JsonResponse(data)

class DepartmentView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id>0:
            departments = list(Departments.objects.filter(id=id).values())
            if len(departments)>0:
                department = departments[0]
                data = {"Department":department}
            else:
                data = {"message":"Department no found"}
            return JsonResponse(data)
        else:
            departments = list(Departments.objects.values())
            if len(departments) >0:
                data = {'Departments':departments}
            else:
                data = {'message':"Departments no found"}
            return JsonResponse(data)

    def post(self, request):
        data = json.loads(request.body)
        department = data['department_name']
        Departments.objects.create(department_name =department)
        return JsonResponse({'Department': department+" upload"})

    def put(self, request, id=0):
        data = json.loads(request.body)
        departments = list(Departments.objects.filter(id=id).values())
        if len(departments)>0:
            department = Departments.objects.get(id=id)
            department.department_name = data['department_name']
            department.save()
            data = {'message':'Deparment edited'}
        else:
            data = {'message':'Department no found'}
        return JsonResponse(data)
        

    def delete(self, request, id=0):
        deparment = list(Departments.objects.filter(id=id).values())
        if len(deparment)>0:
            Departments.objects.filter(id=id).delete()
            data = {"message": "Deparment deleted"}
        else:
            data = {'message':"Deparment no found"}
        return JsonResponse(data)
