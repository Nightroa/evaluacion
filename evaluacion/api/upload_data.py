import psycopg2
import pandas as pd


def conect():

    try:
        conn = psycopg2.connect(
            database = 'xaldigital',
            user = 'root',
            password = 'toor',
            host = 'db')
        
        print("conexion exitosa")
        cursor = conn.cursor()
        return cursor, conn
    except Exception as ex:
        print(ex)
    
def upload_data():
    df = pd.read_csv("./api/Sample (1) (2) (1) (1) (8).csv")
    df = validate_data(df)
    df_department=pd.DataFrame()
    df_city = pd.DataFrame()
    df_company = pd.DataFrame()


    df_city['city'], df_city['state'] = df['city'], df['state']
    df_company['company_name'], df_company['department_id'] =  df['company_name'], df['department']
    df_department['department'] = df['department']
    df_city = df_city.drop_duplicates()
    df_company = df_company.drop_duplicates()
    df_department = df_department.drop_duplicates()
    df = df.drop_duplicates()

    insert(df_city)
    insert(df_department)
    insert(df_company)
    insert(df)

def validate_data(df):
    
    df = pd.DataFrame.from_dict(df)
    df['state'] = df['state'].str.strip(" ")


    if not df['state'].empty:
        df['len_state'] = df['state'].str.len()

    marked = df['len_state']>2

    df_error = df[marked]

    if not df_error.empty:
        df_error.drop(inplace=True, columns=['len_state'])
        print("Error in data input, this rows contains invalid data \n", df_error.head(50), "\n")

    df.drop(inplace=True, index=df.index[df['len_state']>2].tolist(), columns=['len_state'])

    return df



def insert(df):
    psql, conn = conect()
    if not df.empty:
        if len(df.columns)==2 and 'city' in df.columns:
            for n in df.iterrows():
                n =n[1]
                query = """ INSERT INTO citys (city, state) VALUES (%s,%s)"""
                data = n.city,n.state
                psql.execute(query,data)
                conn.commit()


        elif len(df.columns)==2 and 'company_name' in df.columns:
            for n in df.iterrows():
                n =n[1]
                query = "SELECT id FROM departments WHERE department_name = '{}'".format(n.department_id)
                psql.execute(query)
                row = psql.fetchone()
                query = """ INSERT INTO company (company_name, department_id) VALUES (%s, %s)"""
                data = n.company_name, row[0]
                psql.execute(query,data)
                conn.commit()
        
        elif len(df.columns)==1 and 'department' in df.columns:
            for n in df.iterrows():
                n =n[1]
                query = """ INSERT INTO departments (department_name) VALUES (%s)"""
                data = n
                psql.execute(query,data)
                conn.commit()

        else:
            for n in df.iterrows():
                n =n[1]
                query = "SELECT id FROM departments WHERE department_name = '{}'".format(n.department)
                psql.execute(query)
                row_department = psql.fetchone()
                query = "SELECT id FROM citys WHERE city = '{}'".format(n.city)
                psql.execute(query)
                row_citys = psql.fetchone()
                query = "SELECT id FROM company WHERE company_name = '{}'".format(n.company_name)
                psql.execute(query)
                row_companys = psql.fetchone()
                query = """ INSERT INTO employee (first_name,last_name,address,zip,phone1,phone2,email,city_id, company_id, department_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                data =n.first_name,n.last_name,n.address,n.zip,n.phone1,n.phone2,n.email,row_citys[0],row_companys[0],row_department[0]
                psql.execute(query,data)
                conn.commit()
    else:
        pass


upload_data()