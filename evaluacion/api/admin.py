from django.contrib import admin
from .models import Employee, Citys, Company, Departments

# Register your models here.

admin.site.register(Employee)
admin.site.register(Citys)
admin.site.register(Company)
admin.site.register(Departments)