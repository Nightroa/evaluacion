from django.db import models

# Create your models here.

class Citys(models.Model):
    city = models.CharField(max_length=30)
    state = models.CharField(max_length=2)

    class Meta:
        verbose_name = 'Citys'
        verbose_name_plural = 'Citys'
        db_table = 'citys'
        ordering = ['state']

class Departments(models.Model):
    department_name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Departments'
        verbose_name_plural = 'Departments'
        db_table = 'departments'
        ordering = ['department_name']


class Company(models.Model):
    company_name = models.CharField(max_length=50)
    department = models.ForeignKey(Departments, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companys'
        db_table = 'company'
        ordering = ['company_name']

class Employee(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    zip = models.IntegerField(blank=True, null=True)
    phone1 = models.CharField(max_length=30)
    phone2 = models.CharField(max_length=30)
    email = models.CharField(max_length=255)
    city = models.ForeignKey(Citys, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    department = models.ForeignKey(Departments, on_delete=models.CASCADE)


    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'
        db_table = 'employee'
        ordering = ['last_name', 'first_name']

